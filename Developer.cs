using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Xml.Serialization;
using System.IO;


namespace BugRecord
{
    public partial class Developer : Form
    {
        string BugId;
        public SqlConnection conDataBase = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=D:\testing\BugRecord\BugRecord\bin\Debug\BugRecord.mdf;Integrated Security=True;Connect Timeout=30");
        public class Savexml
        {
            public static void SaveData(object obj, string filename)
            {
                XmlSerializer se = new XmlSerializer(obj.GetType());
                TextWriter wr = new StreamWriter(filename);
                se.Serialize(wr, obj);
                wr.Close();
            }
        }
        public Developer()
        {
            InitializeComponent();
            Fillbug();
            Assign();
           
        }
     
        
        void Fillbug()
        {
           
                conDataBase.Open();
                string query = "Select * From Programmer";
             
                SqlCommand cmdDataBase = new SqlCommand(query, conDataBase);
                SqlDataReader dr = cmdDataBase.ExecuteReader();

                while (dr.Read())
                {
                    string title = dr.GetString(1);
                    selectbug_comboBox.Items.Add(title);
                }
                conDataBase.Close();
           

           }
        void Assign()
        {
           

                conDataBase.Open();
                string query = "Select UserName from UserProfile where role='Debugger'";

                SqlCommand cmdDataBase = new SqlCommand(query, conDataBase);
                SqlDataReader dr = cmdDataBase.ExecuteReader();

                while (dr.Read())
                {
                    string assign = dr["UserName"].ToString();
                    assignbugto_combobox.Items.Add(assign);
                }
                conDataBase.Close();           
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           

            }

        private void comboBox1_DropDownClosed(object sender, EventArgs e)
        {
        }

        private void Developer_Load(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            buglist_dataGridView.Visible = true;
            hide.Visible = true;
            
            conDataBase.Open();
            SqlCommand cmd = new SqlCommand("select UserProfile.UserName,Product.ProductTitle,Product.ProductVersion,Programmer.BugTitle,Programmer.BugDesc,Programmer.BugStatus from Product inner join UserProfile on  UserProfile.UserId = Product.UserId inner join Programmer on Programmer.ProductId = Product.ProductId", conDataBase);
            SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmd;
                DataTable dt = new DataTable();
                sda.Fill(dt);
                BindingSource bs = new BindingSource();
                bs.DataSource = dt;
                buglist_dataGridView.DataSource = bs;
                sda.Update(dt);
                
                conDataBase.Close();
           


        }  

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
      
        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

                conDataBase.Open();

                string query = "Select * From Programmer where BugTitle = '"+selectbug_comboBox.Text+ "'";

                SqlCommand cmdDataBase = new SqlCommand(query, conDataBase);
                SqlDataReader dr = cmdDataBase.ExecuteReader();

                while (dr.Read())
                {
                    BugId = dr.GetInt32(0).ToString();
                    string title = dr.GetString(1).ToString();
                    string desc = dr.GetString(2).ToString();
                 
                    bugtitle.Text = title;
                    bugdescription.Text = desc;
                    
                    byte[] img = (byte[])(dr["BugImage"]);  

                    if(img == null)
                        bugimage.Image = null;
                    else
                    {
                        MemoryStream mstream = new MemoryStream(img);
                        bugimage.Image = System.Drawing.Image.FromStream(mstream);
                    }
                    
                }

                conDataBase.Close();

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            conDataBase.Open();
            string assign = assignbugto_combobox.Text;
            string priority = priorityCombo.Text;
          
            SqlCommand cmd = new SqlCommand("INSERT INTO AssignBug (bug_Id,AssignTo,Priority) VALUES (@bugid,@assign,@priority)", conDataBase);

            cmd.Parameters.AddWithValue("@bugid", BugId);
            cmd.Parameters.AddWithValue("@assign", assign);
            cmd.Parameters.AddWithValue("@priority", priority);

                int affectedRows = cmd.ExecuteNonQuery();

                if (affectedRows > 0)
                {
                    MessageBox.Show("Assign Success", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                else
                {
                    MessageBox.Show("Assign Failed", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                }

            conDataBase.Close();


        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            buglist_dataGridView.Visible = false;
        }

        private void bugimage_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
           
                
                string title = bugtitle.Text;
                string desc = bugdescription.Text;


                DataTable dt = new DataTable();
                dt.TableName = "Bug Information";

                
                dt.Columns.Add(title);
                dt.Columns.Add(desc);


                DataSet ds = new DataSet();
                ds.DataSetName = "bugIssues";

                ds.WriteXml("BugFormat.xml");
                

               
           
        }

        private void hide_Click(object sender, EventArgs e)
        {
            buglist_dataGridView.Visible = false;
            hide.Visible = false;
        }

        private void logout_Click(object sender, EventArgs e)
        {
            this.Hide();
            UserProfile open = new UserProfile();
            open.Show();
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            buglist_dataGridView.Visible = true;
            hide.Visible = true;

            conDataBase.Open();
            SqlCommand cmd = new SqlCommand("select Programmer.BugTitle,Programmer.BugDesc, solution.comment from Product inner join UserProfile on  UserProfile.UserId = Product.UserId inner join Programmer on Programmer.ProductId = Product.ProductId inner join solution on solution.BugId = Programmer.BugId", conDataBase);
            SqlDataAdapter sda = new SqlDataAdapter();
            sda.SelectCommand = cmd;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            BindingSource bs = new BindingSource();
            bs.DataSource = dt;
            buglist_dataGridView.DataSource = bs;
            sda.Update(dt);

            conDataBase.Close();
        }

        }
    }

