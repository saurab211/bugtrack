using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace BugRecord
{
    public partial class Debugger : Form
    {
        //Declare Global Variable
        SqlConnection conn;
        string userID;
        string ID;
        string getID;

        public Debugger(string ID)
        {
            InitializeComponent();
            DataGridView();
            userID = ID;
            //set get user ID
            DebuggerConn hey1 = new DebuggerConn();
            hey1.setLoginID(userID);
            getID = hey1.getID();

            //open connection
            conn = hey1.connect();
        }






     


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        private void DataGridView()
        {


        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("select * from Programmer", conn);
            
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.gridview.Rows[e.RowIndex];

                    bugtitle.Text = row.Cells["BugTitle"].Value.ToString();
                    griddesc.Text = row.Cells["BugDesc"].Value.ToString();

                    byte[] img = (byte[])(dr["BugImage"]);

                    if (img == null)
                        Bug_picture.Image = null;
                    else
                    {
                        MemoryStream mstream = new MemoryStream(img);
                        Bug_picture.Image = System.Drawing.Image.FromStream(mstream);
                    }

                }



            }
            conn.Close();




        }


        private void Debugger_Load(object sender, EventArgs e)
        {



        }

        private void submit_Click(object sender, EventArgs e)
        {
            string bugt = bugtitle.Text;
            string bugd = griddesc.Text;

            conn.Open();
          
            SqlCommand cmd = new SqlCommand("select * from Programmer where BugTitle = @title", conn);
           
            cmd.Parameters.AddWithValue("@title", bugt);
            SqlDataReader dr = cmd.ExecuteReader();
            while(dr.Read())
            {
                ID = dr["BugId"].ToString();
                
            }
            conn.Close();
        
      
            conn.Open();

            string solution = suggest.Text;
            SqlCommand cmd1 = new SqlCommand("INSERT INTO solution (BugId,comment) VALUES (@id, @comment)", conn);

            cmd1.Parameters.AddWithValue("@id", ID);
            cmd1.Parameters.AddWithValue("@comment", solution);

           
                int rows = cmd1.ExecuteNonQuery();
                if (rows > 0)
                {
                    MessageBox.Show("Solution Posted");
                }
                else
                {
                    MessageBox.Show("Error");

                }

          
            conn.Close();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            gridview.Visible = true;

            conn.Open();

            SqlCommand cmd = new SqlCommand("Select Programmer.BugTitle,Programmer.BugDesc,AssignBug.Priority From Programmer inner join AssignBug on AssignBug.Bug_id = Programmer.BugId where AssignBug.AssignTo = @email", conn);


            cmd.Parameters.AddWithValue("@email", getID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            BindingSource bs = new BindingSource();
            bs.DataSource = dt;
            gridview.DataSource = bs;
            sda.Update(dt);

            conn.Close();
        }

        private void loggout_Click(object sender, EventArgs e)
        {
            this.Hide();
            UserProfile open = new UserProfile();
            open.Show();
        }

        private void Bug_picture_Click(object sender, EventArgs e)
        {

        }
    }
    public class DebuggerConn
    {
        SqlConnection conxn;

        public string loginID;

        public void setLoginID(string ID)
        {
            loginID = ID;
        }
        public string getID()
        {
            return loginID;
        }

        public SqlConnection connect()
        {
            return conxn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=D:\testing\BugRecord\BugRecord\bin\Debug\BugRecord.mdf;Integrated Security=True;Connect Timeout=30");

        }
    }
}
