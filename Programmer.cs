using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace BugRecord
{
    public partial class Programmer : Form
    {
        string productId;
        string UserId;
        public SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=D:\testing\BugRecord\BugRecord\bin\Debug\BugRecord.mdf;Integrated Security=True;Connect Timeout=30");

        public Programmer(string getId)
        {
            InitializeComponent();
            UserId = getId;
            product();
        }
        public void product()
        {
      
        }
            
       

        private void button1_Click(object sender, EventArgs e)
        {
            
            bugbox.Visible = true;
            productbox.Enabled = false;
       }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Programmer_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
           
        }

        private void insert_Click(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bugbox.Visible = false;
           
            
        }

       

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

        }

        private void logout_Click(object sender, EventArgs e)
        {
            this.Hide();
            UserProfile open = new UserProfile();
            open.Show();
        }

        private void add_Click(object sender, EventArgs e)
        {
            bugbox.Visible = false;
            productbox.Visible = true;
        }

        private void insert_Click_1(object sender, EventArgs e)
        {
            conn.Open();

            string title = text.Text;
            string desc = textBox2.Text;
            string pid = productId;
            string status = statusCombo.Text;

            byte[] imgData;
            imgData = File.ReadAllBytes(textBox3.Text);

            SqlCommand cmd = new SqlCommand("INSERT INTO Programmer (BugTitle,BugDesc,BugImage,ProductId,BugStatus) VALUES (@title, @desc, @DATA, @pID,@status)", conn);
           
            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@desc", desc);
            cmd.Parameters.AddWithValue("@DATA", imgData);
            cmd.Parameters.AddWithValue("@pID", pid);
            cmd.Parameters.AddWithValue("@status", status);


            try
            {
                int affectedRows = cmd.ExecuteNonQuery();
                if (affectedRows > 0)
                {

                    MessageBox.Show("Bug Sucessfylly Inserted", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    productId = null;

                   

                }
                else
                {
                    MessageBox.Show("Bug Insert Failed", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                }

            }
            catch
            {
                MessageBox.Show("Error");
            }
            conn.Close();

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            conn.Open();
            string producttitle = ptitle.Text;
            string version = comboversion.Text;
            string productdesc = pdesc.Text;
            string useid = UserId;

            SqlCommand cmd = new SqlCommand("INSERT INTO Product (ProductTitle,ProductDesc,ProductVersion,UserId) VALUES (@title,@desc,@version,@userId)", conn);
            cmd.Parameters.AddWithValue("@title", producttitle);
            cmd.Parameters.AddWithValue("@desc", productdesc);
            cmd.Parameters.AddWithValue("@version", version);
            cmd.Parameters.AddWithValue("@userId", useid);

         
            try
            {
                int affectedRows = cmd.ExecuteNonQuery();

                if (affectedRows > 0)
                {


                    MessageBox.Show("Product Successfully Inserted", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                    UserId = null;

                }
                else
                {
                    MessageBox.Show("Product Insert Failed", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                }

            }
            catch(SqlException ex)
            {
           
                MessageBox.Show(ex.Message);
            
            }

            conn.Close();

                                                          
        }

        private void browse_Click(object sender, EventArgs e)
        {
            OpenFileDialog filechooser = new OpenFileDialog();
            filechooser.Filter = "image files (*.jpg; *.png; *.gif)|*.jpg; *.png; *.gif|All files(*.*)|*.*";
            filechooser.InitialDirectory = @"C:\Users\Shrestha\Pictures\Camera Roll";
            filechooser.Title = "Select Image for Upload";

            if (filechooser.ShowDialog() == DialogResult.OK)
            {

                textBox3.Text = filechooser.FileName;

            }
        }

        private void productbox_Enter(object sender, EventArgs e)
        {

        }

        private void view_Click(object sender, EventArgs e)
        {
           

        }

        private void prodctCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            conn.Open();
            SqlCommand cmd1 = new SqlCommand("select * from Product where ProductTitle = '" + productCombo.Text + "'", conn);

            SqlDataReader dr = cmd1.ExecuteReader();

            while (dr.Read())
            {
                productId = dr.GetInt32(0).ToString();

                
            }
            conn.Close();
        

        }

        private void bugadd_Click(object sender, EventArgs e)
        {
            bugbox.Visible = true;
            productbox.Enabled = false;

            conn.Open();

            SqlCommand cmd = new SqlCommand("select ProductTitle from Product", conn);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                productCombo.Items.Add(dr["ProductTitle"].ToString());
            }
            conn.Close();
        }

        private void bugbox_Enter(object sender, EventArgs e)
        {

        }

 
    }
}
